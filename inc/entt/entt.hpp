
#pragma once

#include "../../entt/single_include/entt/entt.hpp"

// =============================================================================
// extra stuff for entt

namespace entt
{
	//! reverse lookup - think it's slow AF
	template<class C>
	entt::entity find(entt::registry& r, C* p)
	{
		for (auto [o, c] : r.view<C>().each())
			if (p == &c)
				return o;
		return entt::null;
	}

	template<class C = entt::entity>
	size_t count(entt::registry& registry)
	{
		size_t count = 0;

		registry.view<C>().each([&](auto entity) { count++; });

		return count;
	}
}
